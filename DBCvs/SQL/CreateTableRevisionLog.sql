USE [CVS]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RevisionLog_revDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RevisionLog] DROP CONSTRAINT [DF_RevisionLog_revDate]
END

GO

USE [CVS]
GO

/****** Object:  Table [dbo].[RevisionLog]    Script Date: 02/02/2014 18:51:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RevisionLog]') AND type in (N'U'))
DROP TABLE [dbo].[RevisionLog]
GO

USE [CVS]
GO

/****** Object:  Table [dbo].[RevisionLog]    Script Date: 02/02/2014 18:51:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RevisionLog](
	[revID] [uniqueidentifier] NOT NULL,
	[revDate] [datetime] NOT NULL,
	[revObject] [varchar](255) NOT NULL,
	[revAction] [varchar](255) NOT NULL,
	[revStatement] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_RevisionLog] PRIMARY KEY CLUSTERED 
(
	[revID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[RevisionLog] ADD  CONSTRAINT [DF_RevisionLog_revDate]  DEFAULT (getdate()) FOR [revDate]
GO


