IF  EXISTS (SELECT * FROM sys.triggers WHERE parent_class_desc = 'DATABASE' AND name = N'RevisionLogTrigger')
DISABLE TRIGGER [RevisionLogTrigger] ON DATABASE

GO

USE [FacturacaoAmbimed]
GO

/****** Object:  DdlTrigger [RevisionLogTrigger]    Script Date: 02/02/2014 18:49:58 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE parent_class_desc = 'DATABASE' AND name = N'RevisionLogTrigger')DROP TRIGGER [RevisionLogTrigger] ON DATABASE
GO

USE [FacturacaoAmbimed]
GO

/****** Object:  DdlTrigger [RevisionLogTrigger]    Script Date: 02/02/2014 18:49:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE TRIGGER [RevisionLogTrigger] 
ON DATABASE 
FOR DDL_DATABASE_LEVEL_EVENTS 
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @data XML;
    DECLARE @ActionType nvarchar(max);
    DECLARE @Object nvarchar(max);
    DECLARE @Statement nvarchar(max);
    SET @data = EVENTDATA();
    SET @Object = @data.value('(/EVENT_INSTANCE/ObjectName)[1]', 'nvarchar(255)');
    SET @ActionType = @data.value('(/EVENT_INSTANCE/EventType)[1]', 'nvarchar(255)');
    SET @Statement = @data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'nvarchar(max)'); 

        BEGIN TRAN;
        INSERT INTO CVS.dbo.RevisionLog 
            (
             revID,
             revObject,
             revAction,
             revStatement
            ) 
        VALUES 
            (
				newid(),
				@Object,
				@ActionType,
				@Statement
            );
            
         exec CVS.dbo.save2file 'C:\DBCVS\FacturacaoAmbimed\', @Object,@Statement
         
        COMMIT TRAN;
END




GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

DISABLE TRIGGER [RevisionLogTrigger] ON DATABASE
GO

ENABLE TRIGGER [RevisionLogTrigger] ON DATABASE
GO


