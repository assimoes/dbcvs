﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [DBCvs] (Assembly)
       [dbo].[save2file] (Procedure)

** Supporting actions
