//------------------------------------------------------------------------------
// <copyright file="CSSqlStoredProcedure.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.IO;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void save2file(SqlString repository, SqlString objectname, SqlString contents)
    {

        string repo = repository.ToString();
        bool exists = Directory.Exists(repo);

        if (!exists)
        {
            Directory.CreateDirectory(repo);
        }
        Console.WriteLine(repo);


        string filename = "dbo." + objectname.ToString() + ".sql";
        string[] lines = contents.ToString().Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        StreamWriter sw = null;
        try
        {


            sw = File.CreateText(repo + filename);

            foreach (string item in lines)
            {
                sw.WriteLine(item);
            }

            sw.Close();

        }
        catch (Exception ex)
        {
            sw.Close();
            sw.Dispose();
            SqlContext.Pipe.Send("ERROR: " + ex.Message);
        }
        finally
        {
            sw.Close();
            sw.Dispose();
        }

    }
}
